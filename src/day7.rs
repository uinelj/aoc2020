use itertools::Itertools;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
// use crate::solvable::Solvable;

#[derive(Debug)]
pub struct Day7 {
    data: Vec<String>,
}

// struct Bag {
//     color: String,
//     content: Option<Box<Bag>>,
// }

// impl Bag {

// }
// bag color -> types of bags contained
// part 2 will likely involve changing the hashset<string> to some hashmap<string, u32>
type Bags = HashMap<String, Option<HashSet<String>>>;

impl Day7 {
    pub fn load(path: &str) -> Self {
        let expenses = fs::read_to_string(path)
            .expect("file doesn't exist")
            .split('\n')
            .map(|s| s.to_string())
            .collect();

        Self { data: expenses }
    }

    pub fn solve(self: &Self) -> Option<i64> {
        // self.data.iter().map(|line| {
        //     dbg!(line.split(" ").collect::<Vec<_>>());
        // });
        let mut bags = Bags::new();
        for line in &self.data {
            // split bag data
            let splitted: Vec<&str> = line.split(" ").collect();

            // if fifth token is no, bag is empty
            if splitted[4] == "no" {
                bags.insert(splitted[0..2].join(" "), None);
            } else {
                // we get bag color
                let bag_color = splitted[0..2].join(" ");

                // we get bag types that are contained
                let contained_bags = splitted.windows(2).skip(5).step_by(4).map(|x| x.join(" "));

                // we get which bags are containable in each other
                match bags.get_mut(&bag_color) {
                    Some(set) => println!("{:?}", set),
                    None => {
                        let new_set: HashSet<String> = contained_bags.collect();
                        bags.insert(bag_color, Some(new_set));
                    }
                }
            }
        }

        let a = match bags.get("muted yellow").unwrap() {
            Some(v) => v,
            None => panic!("uhh"),
        };

        let mut counter = 0;

        // fn check_contains(bags: Bags, key: String) -> bool {
        //     match bags.get(&key) {
        //         Some(set) => {
        //             if set.contains("shiny gold") {
        //                 return true;
        //             } else {
        //                 let keys
        //                 for
        //             }

        //         }
        //     }
        // }

        for (bag, contained) in bags.iter() {
            match contained {
                Some(set) => {
                    if set.contains("shiny gold") {
                        counter += 1;
                    } else {
                    }
                }
                None => (),
            }
        }
        // println!("{:?}", bags.get("bright white"));
        // dbg!(bags);
        None
    }

    pub fn solve2(self: &Self) -> Option<i64> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day1part1() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve(), Some(1007331));
    }

    #[test]
    fn day1part2() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve2(), Some(48914340))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve2()
        })
    }
}
