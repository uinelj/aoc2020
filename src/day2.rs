use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
use std::ops::Range;
use std::str::FromStr;
use std::string::ParseError;
// use crate::solvable::Solvable;

#[derive(Debug)]
pub struct Day2 {
    data: Vec<Password>,
}

#[derive(PartialEq, Debug)]
struct Password {
    constraint_letter: char,
    occurrence_range: Range<usize>,
    password: String,
}

impl FromStr for Password {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parsed: Vec<&str> = s.split(|c| c == '-' || c == ':' || c == ' ').collect();
        let min = parsed[0].parse::<usize>().expect("int expected");
        let max = parsed[1].parse::<usize>().expect("int expected");
        Ok(Password {
            constraint_letter: parsed[2].parse::<char>().expect("char expected"),
            occurrence_range: (min..max + 1),
            password: parsed[4].to_string(),
        })
    }
}

impl Password {
    pub fn is_valid(self: &Self) -> bool {
        self.occurrence_range
            .contains(&self.password.matches(self.constraint_letter).count())
    }
}

impl Day2 {
    pub fn load(path: &str) -> Self {
        let passwords = fs::read_to_string(path)
            .expect("file doesn't exist")
            .split('\n')
            .map(|x| Password::from_str(x).expect("ill formatted string"))
            .collect();

        Day2 { data: passwords }
    }

    pub fn solve(self: &Self) -> Option<usize> {
        // we take data, iterate on each passwor to see if they are valid
        Some(
            self.data
                .iter()
                .filter(|pw| pw.is_valid())
                .collect::<Vec<&Password>>()
                .len(),
        )
    }

    pub fn solve2(self: &Self) -> Option<usize> {
        // for each password, we get indices of chars to check
        // then we fetch the chars and see if one XOR the other are matching the constraint letter
        let mapped: Vec<_> = self
            .data
            .iter()
            .filter_map(|x| {
                let indices: Vec<usize> =
                    vec![x.occurrence_range.start - 1, x.occurrence_range.end - 2];
                let password_chars: Vec<char> = x.password.chars().collect();
                let chars: Vec<char> = indices
                    .into_iter()
                    .map(move |index| password_chars[index])
                    .collect();
                if (chars[0] == x.constraint_letter) ^ (chars[1] == x.constraint_letter) {
                    return Some(true);
                }
                None
            })
            .collect();

        Some(mapped.len())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day1part1() {
        let day1 = Day2::load("res/2.txt");
        assert_eq!(day1.solve(), Some(418));
    }

    #[test]
    fn day1part2() {
        let day1 = Day2::load("res/2.txt");
        assert_eq!(day1.solve2(), Some(616))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day1 = Day2::load("res/2.txt");
        b.iter(|| day1.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day2::load("res/2.txt");
            day1.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day1 = Day2::load("res/2.txt");
        b.iter(|| day1.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day2::load("res/2.txt");
            day1.solve2()
        })
    }
}
