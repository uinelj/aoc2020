use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
use std::ops::Range;
use std::str::FromStr;
use std::string::ParseError;
// use crate::solvable::Solvable;

lazy_static! {
    static ref REQUIRED_FIELDS: HashSet<&'static str> =
        HashSet::from_iter(vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]);
}

#[derive(Debug)]
pub struct Day4 {
    passports: HashSet<String>,
}

impl FromStr for Day4 {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let passports = s.split("\n\n").map(|x| x.to_string()).collect();
        Ok(Day4 { passports })
    }
}

impl Day4 {
    pub fn load(path: &str) -> Self {
        Day4::from_str(&fs::read_to_string(path).expect("file doesn't exist"))
            .expect("could not load day3")
    }

    pub fn solve(self: &Self) -> Option<usize> {
        let valid_passowrds = self
            .passports
            .iter()
            .filter_map(|passport| {
                match passport
                    .split(|c| c == '\n' || c == ' ' || c == ':')
                    .collect::<HashSet<&str>>()
                    .is_superset(&REQUIRED_FIELDS)
                {
                    true => Some(()),
                    false => None,
                }
            })
            .count();
        Some(valid_passowrds)
    }

    pub fn solve2(self: &Self) -> Option<usize> {
        fn validate_field(field: &str, value: &str) -> bool {
            let ret = match field {
                "byr" => (1920..2003).contains(&value.parse::<i32>().unwrap()),
                "iyr" => (2010..2021).contains(&value.parse::<i32>().unwrap()),
                "eyr" => (2020..2031).contains(&value.parse::<i32>().unwrap()),
                "hgt" => {
                    let numeric_value: &str = value
                        .split(|c: char| !c.is_numeric())
                        .next()
                        .expect("numeric value not found");
                    return match value.chars().rev().take(2).collect::<String>().as_str() {
                        "mc" => (150..194).contains(&numeric_value.parse::<i32>().unwrap()),
                        "ni" => (59..77).contains(&numeric_value.parse::<i32>().unwrap()),
                        _ => false,
                    };
                }
                "hcl" => {
                    if value.starts_with("#") {
                        if value.chars().count() == 7 {
                            //FIXME: find if RGB
                            if value
                                .chars()
                                .skip(1)
                                .all(|x| ('0'..'A').contains(&x) || ('a'..'g').contains(&x))
                            {
                                return true;
                            }
                        }
                    }
                    false
                }
                "ecl" => {
                    let possibilities: HashSet<&str> =
                        HashSet::from_iter(vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]);

                    possibilities.contains(&value)
                }
                "pid" => ((value.chars().count() == 9) && (value.parse::<i32>().is_ok())),
                "cid" => true,
                _ => panic!("unknown field {}", field),
            };

            return ret;
        }

        let valid_passowrds = self.passports.iter().filter_map(|passport| {
            //todo: understand why?
            let replaced = passport.replace("\n", " ");
            let parsed: Vec<&str> = replaced.split(|c| c == ':' || c == ' ').collect();

            if HashSet::from_iter(parsed.clone().into_iter()).is_superset(&REQUIRED_FIELDS) {
                return match parsed
                    .into_iter()
                    .collect::<Vec<&str>>()
                    .chunks(2)
                    .all(|t| validate_field(t[0], t[1]))
                {
                    true => Some(()),
                    false => None,
                };
            }
            return None;
        });

        Some(valid_passowrds.count())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day3part1() {
        let day4 = Day4::load("res/3.txt");
        assert_eq!(day4.solve(), Some(286));
    }

    #[test]
    fn day1part2() {
        let day3 = Day3::load("res/3.txt");
        assert_eq!(day3.solve2(), Some(3638606400))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day3 = Day3::load("res/3.txt");
        b.iter(|| day3.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day3 = Day3::load("res/3.txt");
            day3.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day3 = Day3::load("res/3.txt");
        b.iter(|| day3.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day3 = Day3::load("res/3.txt");
            day3.solve2()
        })
    }
}
