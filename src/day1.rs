use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
// use crate::solvable::Solvable;

#[derive(Debug)]
pub struct Day1 {
    data: Vec<i64>,
}

impl Day1 {
    pub fn load(path: &str) -> Self {
        let expenses = fs::read_to_string(path)
            .expect("file doesn't exist")
            .split('\n')
            .map(|x| x.parse::<i64>().expect("non integer in list"))
            .collect();

        Day1 { data: expenses }
    }

    pub fn solve(self: &Self) -> Option<i64> {
        let data_hash: HashSet<&i64> = HashSet::from_iter(&self.data);
        for entry in &self.data {
            if data_hash.contains(&(2020 - entry)) {
                return Some(entry * (2020 - entry));
            }
        }
        None
    }

    pub fn solve2(self: &Self) -> Option<i64> {
        let pairs = self.data.iter().cartesian_product(self.data.iter());
        let data_hash: HashSet<&i64> = HashSet::from_iter(&self.data);
        for pair in pairs {
            let pairsum = pair.0 + pair.1;
            if pairsum < 2020 && data_hash.contains(&(2020 - pairsum)) {
                return Some(pair.0 * pair.1 * (2020 - pairsum));
            }
        }
        // let a = self.data.iter().combinations(10);
        // println!("{:?}", a);

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day1part1() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve(), Some(1007331));
    }

    #[test]
    fn day1part2() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve2(), Some(48914340))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve2()
        })
    }
}
