use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
use std::ops::Range;
use std::str::FromStr;
use std::string::ParseError;
// use crate::solvable::Solvable;

#[derive(Debug, PartialEq)]
enum Terrain {
    Empty,
    Tree,
}

#[derive(Debug)]
pub struct Day3 {
    map: Vec<Vec<Terrain>>,
}

impl FromStr for Day3 {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let map: Vec<Vec<Terrain>> = s
            .split("\n")
            .map(|x| {
                let terrain = x
                    .chars()
                    .map(|y| match y {
                        '.' => Terrain::Empty,
                        '#' => Terrain::Tree,
                        _ => panic!("unexpected char"),
                    })
                    .collect();

                terrain
            })
            .collect();

        Ok(Day3 { map })
    }
}

impl Day3 {
    pub fn load(path: &str) -> Self {
        Day3::from_str(&fs::read_to_string(path).expect("file doesn't exist"))
            .expect("could not load day3")
    }

    pub fn solve(self: &Self) -> Option<usize> {
        // it's [line][column].
        let increment = (3, 1);
        let position = (0, 0);
        let mut tree_count = 0;
        let map_cycling = self.map.iter().map(|line| line.iter().cycle());
        for (index, line) in map_cycling.enumerate() {
            if line.skip(3 * index).next().expect("wtf?") == &Terrain::Tree {
                tree_count += 1;
            }
        }
        Some(tree_count)
    }

    pub fn solve2(self: &Self) -> Option<usize> {
        let increments = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        let position = (0, 0);
        let mut tree_counts = vec![0; increments.len()];
        for (increment, tree_count) in increments.iter().zip(tree_counts.iter_mut()) {
            let map_cycling = self.map.iter().map(|line| line.iter().cycle());
            for (index, line) in map_cycling.enumerate().step_by(increment.1) {
                if line
                    .skip(increment.0 * (index / increment.1))
                    .next()
                    .expect("wtf?")
                    == &Terrain::Tree
                {
                    *tree_count += 1;
                }
            }
        }
        println!("{:?}", tree_counts);
        println!("{:?}", tree_counts.iter().fold(1, |x, acc| x * acc));
        Some(tree_counts[0])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day3part1() {
        let day3 = Day3::load("res/3.txt");
        assert_eq!(day3.solve(), Some(286));
    }

    #[test]
    fn day1part2() {
        let day3 = Day3::load("res/3.txt");
        assert_eq!(day3.solve2(), Some(3638606400))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day3 = Day3::load("res/3.txt");
        b.iter(|| day3.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day3 = Day3::load("res/3.txt");
            day3.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day3 = Day3::load("res/3.txt");
        b.iter(|| day3.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day3 = Day3::load("res/3.txt");
            day3.solve2()
        })
    }
}
