use std::collections::HashSet;
use std::convert::TryInto;
use std::fs;

#[derive(Debug)]
enum Opcode {}

type Instruction = (Opcode, i64);

#[derive(Debug)]
pub struct Day8 {
    instructions: Vec<(String, i64)>,
}

impl Day8 {
    pub fn load(path: &str) -> Self {
        let instructions = fs::read_to_string(path)
            .expect("file doesn't exist")
            .split('\n')
            .map(|s| {
                let parsed: Vec<&str> = s.split(" ").take(2).collect();
                (
                    parsed[0].to_string(),
                    parsed[1].parse::<i64>().expect("integer expected"),
                )
            })
            .collect();

        Self { instructions }
    }

    pub fn solve(self: &Self) -> Option<i64> {
        // let mut executed : HashSet<(&String, &i64)> = HashSet::new();
        let mut executed: HashSet<usize> = HashSet::new();
        println!("{:?}", self.instructions);
        let mut pc: usize = 0;
        let mut acc: i64 = 0;
        loop {
            let (instruction, value) = self.instructions.get(pc).expect("overflow");
            if executed.contains(&pc) {
                return Some(acc);
            }
            executed.insert(pc);
            println!("PC:{} : {:?} \t(acc: {})", pc, (instruction, value), acc);
            match (instruction.as_str(), value) {
                ("nop", _) => pc += 1,
                ("acc", v) => {
                    acc += value;
                    pc += 1
                }
                ("jmp", v) => {
                    // i think as is not good tho
                    pc = (pc as i64 + value).try_into().expect("pc can't be <0");
                }
                _ => panic!("wrong opcode provided"),
            }
        }
    }

    pub fn solve2(self: &mut Self) -> Option<i64> {
        // let mut executed : HashSet<(&String, &i64)> = HashSet::new();
        let mut executed: HashSet<usize> = HashSet::new();
        let mut already_tried: HashSet<usize> = HashSet::new();

        // println!("{:?}", self.instructions);
        let mut pc: usize = 0;
        let mut acc: i64 = 0;
        let program_size = self.instructions.len();

        loop {
            let swap_index = self.instructions
                .iter()
                .skip(*already_tried.iter().max().unwrap_or(&0))
                .position(|x| (x.0.as_str() == "nop" || x.0.as_str() == "jmp"))
                .expect("no nop || jmp found");
            
            let (instruction, value) = self.instructions.get(swap_index).expect("overflow");
            self.instructions[swap_index] = match (instruction.as_str(), value) {
                ("nop", e) => ("jmp".to_string(), *e),
                ("jmp", e) => ("nop".to_string(), *e),
                _ => panic!("that shouldn't happen")
            };
            
            loop {
                // end is if we try to execute
                // an instruction that is
                // at the very end of the program
                if pc == program_size {
                    // it is the end!
                }
                let (instruction, value) = self.instructions.get(pc).expect("overflow");
                if executed.contains(&pc) {
                    return Some(acc);
                }
                executed.insert(pc);
                println!("PC:{} : {:?} \t(acc: {})", pc, (instruction, value), acc);
                match (instruction.as_str(), value) {
                    ("nop", _) => pc += 1,
                    ("acc", v) => {
                        acc += value;
                        pc += 1
                    }
                    ("jmp", v) => {
                        // i think as is not good tho
                        pc = (pc as i64 + value).try_into().expect("pc can't be <0");
                    }
                    _ => panic!("wrong opcode provided"),
                }
            }
        }
    }
}
