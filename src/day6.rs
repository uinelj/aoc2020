use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::iter::FromIterator;
// use crate::solvable::Solvable;

#[derive(Debug)]
pub struct Day6 {
    data: Vec<String>,
}

impl Day6 {
    pub fn load(path: &str) -> Self {
        let expenses = fs::read_to_string(path)
            .expect("file doesn't exist")
            .split("\n\n")
            .map(|x| x.to_string())
            .collect();

        Day6 { data: expenses }
    }

    pub fn solve(self: &Self) -> Option<usize> {
        println!("{:?}", self);

        let processed: usize = self
            .data
            .iter()
            .map(|group| HashSet::<char>::from_iter(group.replace('\n', "").chars()).len())
            .sum();
        Some(processed)
    }

    // pub fn solve2(self: &Self) -> Option<usize> {
    //     let processed: usize = self
    //         .data
    //         .iter()
    //         .map(|group| {
    //             let splitted: Vec<_> = group.split('\n').collect();
    //             if splitted.len() > 1 {
    //                 let required: HashSet<char> = HashSet::from_iter(splitted[0].chars());
    //                 let hh : HashSet<char> = HashSet::from_iter(splitted[0].chars());
    //                 dbg!(splitted[0]);
    //                 dbg!(hh.intersection(&required));
    //                 // if splitted
    //                 //     .iter()
    //                 //     .all(|answers| required == HashSet::from_iter(answers.chars()))
    //                 // {
    //                 //     splitted.len()
    //                 // } else {
    //                 //     0
    //                 // }
    //                 // println!("oi");
    //                 splitted.iter().map(|x| HashSet::from_iter(x.chars()).intersection(&required)).collect();
    //                 1
    //             } else {
    //                 splitted.len()
    //             }
    //         })
    //         .sum();

    //     Some(processed)
    // }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn day1part1() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve(), Some(1007331));
    }

    #[test]
    fn day1part2() {
        let day1 = Day1::load("res/1.txt");
        assert_eq!(day1.solve2(), Some(48914340))
    }

    #[bench]
    fn bench_step1(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve())
    }

    #[bench]
    fn bench_step1_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve()
        })
    }

    #[bench]
    fn bench_step2(b: &mut Bencher) {
        let day1 = Day1::load("res/1.txt");
        b.iter(|| day1.solve2())
    }

    #[bench]
    fn bench_step2_load(b: &mut Bencher) {
        b.iter(|| {
            let day1 = Day1::load("res/1.txt");
            day1.solve2()
        })
    }
}
