#![feature(test)]
#![feature(split_inclusive)]
extern crate test;

#[macro_use]
extern crate lazy_static;

mod day1;
mod day2;
mod day3;
mod day4;
mod day6;
mod day7;
mod day8;
mod solvable;
fn main() {
    // let day1 = day1::Day1::load("res/1.txt");
    // let day2 = day2::Day2::load("res/2.txt");
    // let day3 = day3::Day3::load("res/3.txt");
    // let day4 = day4::Day4::load("res/4.txt");
    // let day6 = day6::Day6::load("res/6.txt");
    // let day7 = day7::Day7::load("res/7.txt");
    let day8 = day8::Day8::load("res/8.txt");
    // println!("{:?}", day1);
    // println!("{:?}", day1.solve());
    // println!("{:?}", day1.solve2());
    // println!("{:?}", day2);
    // println!("{:?}", day2.solve());
    // println!("{:?}", day2.solve2());
    // println!("{:?}", day3.solve());
    // println!("{:?}", day3.solve2());
    // println!("{:?}", day6.solve());
    println!("{:?}", day8.solve());
}
